import { ListItem } from "./list-item.model";
import { v4 as uuid } from 'uuid';

export class List {

  id: string;
  title: string;
  createdAt: Date;
  finishedAt: Date;
  complete: boolean;
  items: ListItem[];

  constructor(title: string) {
    this.title = title;
    this.createdAt = new Date();
    this.complete = false;
    this.items = [];
    this.id = uuid();
  }

}

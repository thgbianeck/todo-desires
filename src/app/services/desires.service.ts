import { Injectable } from '@angular/core';
import { List } from '../models/list.model';

@Injectable({
  providedIn: 'root'
})
export class DesiresService {

  lists: List[] = []

  constructor() {
    const list1 = new List('Recoletar as pedras do infinito');
    const list2 = new List('Matar todos os heróis');

    this.lists.push(list1, list2);
    console.log(this.lists);
  }

  async getLists(): Promise<List[]> {
    return await this.lists;
  }
}

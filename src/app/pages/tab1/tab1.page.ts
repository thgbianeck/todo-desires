import { Component } from '@angular/core';
import { List } from 'src/app/models/list.model';
import { DesiresService } from 'src/app/services/desires.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  lists: List[] = [];

  constructor(public desiresService: DesiresService) {
    desiresService.getLists().then((list: List[]) => {
      this.lists = list;
    });
  }

}
